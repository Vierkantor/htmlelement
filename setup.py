from setuptools import setup

setup(name='htmlelement',
      version='0.1',
      description='Classes for generating HTML',
      url='https://gitlab.com/Vierkantor/htmlelement',
      author='Vierkantor',
      author_email='vierkantor@vierkantor.com',
      license='AGPL',
      packages=['htmlelement'],
      zip_safe=False)

