"""htmlelement - library for generating html"""

from html import escape

class HTMLSafeString(str):
    def render(self):
        return self
safe = HTMLSafeString

class HTMLUnsafeString(str):
    def render(self):
        return HTMLSafeString(escape(self))
unsafe = HTMLUnsafeString

def render(element):
    """Transform a HTML document to a string."""
    try:
        return element.render()
    except AttributeError as e:
        if isinstance(element, str):
            raise TypeError("use `safe('bla')` or `unsafe('bla')` instead of a bare `str`") from e
        raise

class HTMLElement:
    def __init__(self, tag_name, children=None):
        self.tag_name = tag_name
        if children:
            self.children = children
        else:
            self.children = []
        self.attributes = {}

    def add(self, *children):
        """Add some child elements to this one."""
        for element in children:
            if isinstance(element, str):
                raise TypeError("use `safe('bla')` or `unsafe('bla')` instead of a bare `str`")
        self.children.extend(children)

    def render(self):
        """Transform this HTMLElement to a string.

        This is essentially an implementation detail of `render`.
        You should call the top-level `render` function instead,
        which will have nicer error handling.
        """
        attributes = ' '.join('{}="{}"'.format(key, value) for key, value in self.attributes.items())
        children_rendered = ''.join([render(child) for child in self.children])
        return '<{tag} {attributes}>{children}</{tag}>'.format(
                tag=self.tag_name,
                attributes=attributes,
                children=children_rendered,
        )

    def __str__(self):
        return self.render()

class HTMLAnchor(HTMLElement):
    def __init__(self, href, *children):
        super().__init__('a', children)
        self.attributes['href'] = href

class HTMLDiv(HTMLElement):
    def __init__(self, *children):
        super().__init__('div', children)

class HTMLForm(HTMLElement):
    def __init__(self, action, method, *children):
        super().__init__('form', children)
        self.attributes['action'] = action
        self.attributes['method'] = method

class HTMLHeading(HTMLElement):
    levels = {
            1: 'h1',
            2: 'h2',
            3: 'h3',
            4: 'h4',
            5: 'h5',
            6: 'h6',
    }
    def __init__(self, level, *children):
        super().__init__(HTMLHeading.levels[level], children)

class HTMLInput(HTMLElement):
    def __init__(self, type, name, value, *children):
        super().__init__('input', children)
        self.attributes['type'] = type
        self.attributes['name'] = name
        if value is not None:
            self.attributes['value'] = value

    @classmethod
    def dropdown(cls, label, name, values):
        return HTMLLabel(label, HTMLSelect(name, values))

    @classmethod
    def hidden(cls, name, value):
        return cls('hidden', name, value)

    @classmethod
    def submit(cls, value, name="submit"):
        return cls('submit', name, value)

    @classmethod
    def text(cls, label, name, value=None):
        return HTMLLabel(label, cls('text', name, value))

class HTMLLabel(HTMLElement):
    def __init__(self, *children):
        super().__init__('label', children)

class HTMLOption(HTMLElement):
    def __init__(self, value, *children):
        super().__init__('option', children)
        self.attributes['value'] = value

class HTMLPage:
    def __init__(self):
        self.head = HTMLElement('head')
        self.body = HTMLElement('body')
        self.root = HTMLElement('html', [self.head, self.body])

    def add(self, *children):
        self.body.add(*children)

    def render(self):
        return '<!doctype html>' + str(self.root)

    def __str__(self):
        return self.render()

class HTMLParagraph(HTMLElement):
    def __init__(self, *children):
        super().__init__('p', children)

class HTMLSelect(HTMLElement):
    def __init__(self, name, values):
        super().__init__('select')
        self.attributes['name'] = name
        for key, value in values.items():
            self.add(HTMLOption(key, value))

class HTMLSpan(HTMLElement):
    def __init__(self, *children):
        super().__init__('span', children)

class HTMLTable(HTMLElement):
    def __init__(self, *children):
        super().__init__('table', children)

class HTMLTableBody(HTMLElement):
    def __init__(self, *children):
        super().__init__('tbody', children)

class HTMLTableCell(HTMLElement):
    def __init__(self, *children):
        super().__init__('td', children)

class HTMLTableHeadCell(HTMLElement):
    def __init__(self, *children):
        super().__init__('th', children)

class HTMLTableHead(HTMLElement):
    def __init__(self, *children):
        super().__init__('thead', children)

class HTMLTableRow(HTMLElement):
    def __init__(self, *children):
        super().__init__('tr', children)
